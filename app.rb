require 'sinatra'
require 'sinatra/base'
require 'roar'
require 'representable'
require 'representable/json'
require 'roar/representer/json/hal'
require 'sinatra/reloader' if development?

#require 'sinatra/reloader' if development?
class Article
 attr_accessor :title,:content,:author,:date_created

 def initialized(title,content,author)
  @title = title
  @content = content
  @author  = author
  @date_created = Time.now
 end
 
 def initialize(options ={})
    if options
     @title = options[:title]
     @content = options[:content]
     @author = options[:author]
     @date_created = Time.now 
   end
 end

end
class Articles
 attr_accessor :articles

 def initialize
  @articles = []
 end
end

class ArticleRepresenterV1 < Representable::Decorator
  include Representable::JSON
  include Roar::Representer::JSON::HAL::Links
 
  property :title
  property :content
  property :author

end

module ArticlesLonelyRepresenterV1
  include Representable::JSON::Collection

  #items extend: SongRepresenter, class: Song
  items :class => Article, :decorator => ArticleRepresenterV1 
end

class ArticlesRepresenterV1 < Representable::Decorator
  include Representable::JSON
  #include Roar::Representer::JSON::HAL::Links
 
  #collection :articles extend: SongRepresenter
  collection :articles, :class => Article, :decorator => ArticleRepresenterV1 
end

class ArticleRepresenterV1 < Representable::Decorator
  include Representable::JSON
  #include Roar::Representer::JSON::HAL::Links

 
  property :title
  property :content
  property :author
  property :date_created

end


class App < Sinatra::Base

 get '/?' do
   "test roar"
 end

 get '/api/1/articles' do
  article1 = Article.new(title: "first of many",content: "first of the first of many more to come...",author: "Khaya ka Ndlovu")
  article2 = Article.new(title: "one",content:"Yadda yadda .....",author:"Khaya ka Ndlovu")
  articles = Articles.new
  articles.articles << article1
  articles.articles << article2
  ArticlesRepresenterV1.new(articles).to_json 
 end

get '/api/2/articles' do
  article1 = Article.new(title:"first of many",content: "first of the first of many more to come...")
  article2 = Article.new(title: "one",content:"Yadda yadda ...")
  articles = []
  articles << article1 << article2
  #ArticlesLonelyRepresenterV1.new(articles).to_json
  [200, {'Content-Type' => 'application/json'},articles.extend(ArticlesLonelyRepresenterV1).to_json]
 end

end
